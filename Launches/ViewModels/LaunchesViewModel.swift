//
//  LaunchesViewModel.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

protocol LaunchesViewModelDelegate: class {
    func onFetchCompleted()
    func onFetchFailed(with reason: String)
}

class LaunchesViewModel {
    private weak var delegate: LaunchesViewModelDelegate?
    
    let client = LaunchClient()
    let request: LaunchRequest
    
    private var launches: [Launch] = []
    
    init(request: LaunchRequest, delegate: LaunchesViewModelDelegate) {
        self.request = request
        self.delegate = delegate
    }
    
    func launch(at index: Int) -> Launch {
        return self.launches[index]
    }
    
    func count() -> Int {
        return self.launches.count
    }
    
    func fetchLaunches() {
        client.fetchLaunches(with: request) { [weak self] Result in
            guard let strongSelf = self else {
                return
            }
            
            switch Result {
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.delegate?.onFetchFailed(with: error.reason)
                }
            case .success(let response):
                DispatchQueue.main.async {
                    strongSelf.launches.append(contentsOf: response.launches)
                    strongSelf.delegate?.onFetchCompleted()
                }
            }
        }
    }
}
