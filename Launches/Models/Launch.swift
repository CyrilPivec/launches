//
//  Launch.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

struct Launch: Decodable {
    let id: Int
    let name: String
    let status: Int
    let windowstart: String
    let location: Location
    let rocket: Rocket
    let lsp: Lsp
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case status
        case windowstart
        case location
        case rocket
        case lsp
    }
    

}
