//
//  Pad.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

struct Pad: Decodable {
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name
    }
}
