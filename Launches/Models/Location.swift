//
//  Location.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

struct Location: Decodable {
    let pads: [Pad]
    
    enum CodingKeys: String, CodingKey {
        case pads
    }
}
