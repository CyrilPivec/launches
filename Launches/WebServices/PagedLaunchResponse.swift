//
//  PagedLaunchResponse.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

struct PagedLaunchResponse: Decodable {
    let launches: [Launch]
    
    enum CodingKeys: String, CodingKey {
        case launches
    }
}
