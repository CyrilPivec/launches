//
//  DataResponseError.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

enum DataResponseError: Error {
    case network
    case decoding
    
    var reason: String {
        switch self {
        case .network:
            return "Error while fetching data"
        case .decoding:
            return "Error while decoding data"
        }
    }
}
