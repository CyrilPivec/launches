//
//  LaunchClient.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import Foundation

class LaunchClient {
    private lazy var baseURL: URL = {
        return URL(string: "http://launchlibrary.net/1.4/")!
    }()
    
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchLaunches(with request: LaunchRequest, completion: @escaping (Result<PagedLaunchResponse, DataResponseError>) -> Void) {
        
        let urlRequest = URLRequest(url: self.baseURL.appendingPathComponent(request.path))
        
        session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data else {
                completion(Result.failure(DataResponseError.network))
                return
            }
            
            guard let decodedResponse = try? JSONDecoder().decode(PagedLaunchResponse.self, from: data) else {
                completion(Result.failure(DataResponseError.decoding))
                return
            }
            
            completion(Result.success(decodedResponse))
        }).resume()
    }
}
