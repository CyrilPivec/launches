//
//  LaunchViewController.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var rocketNameLabel: UILabel!
    @IBOutlet var rocketConfigurationLabel: UILabel!
    @IBOutlet var padNameLabel: UILabel!
    @IBOutlet var lspNameLabel: UILabel!
    @IBOutlet var lspCountryLabel: UILabel!
    @IBOutlet var favButton: UIButton!
    
    var launch: Launch?
    var favorite: Bool = false
    
    @objc static func controllerFromStoryboard() -> LaunchViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let launchViewController = storyboard.instantiateViewController(withIdentifier: "LaunchViewController") as? LaunchViewController ?? LaunchViewController()
        return launchViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let launch = self.launch {
            self.configure(with: launch)
        }
    }
    
    func configure(with launch: Launch) {
        self.nameLabel.text = launch.name
        self.dateLabel.text = launch.windowstart
        self.rocketNameLabel.text = "Name : " + launch.rocket.name
        self.rocketConfigurationLabel.text = "Configuration : " + launch.rocket.configuration
        self.padNameLabel.text = "Name : " + launch.location.pads[0].name
        self.lspNameLabel.text = "Name : " + launch.lsp.name
        self.lspCountryLabel.text = "Country : " + launch.lsp.countryCode
        self.setFavButton(with: launch.id)
    }
    
    func setFavButton(with id: Int) {
        let defaults = UserDefaults.standard
        if defaults.value(forKey: String(id)) != nil {
            self.favButton.setTitle("Unfavorite", for: .normal)
            self.favorite = true
        } else {
            self.favButton.setTitle("Favorite", for: .normal)
            self.favorite = false
        }
    }
    
    @IBAction func setFavorite(_ sender: Any) {
        if let launch = self.launch {
            let defaults = UserDefaults.standard
            if !favorite {
                defaults.set(true, forKey: String(launch.id))
                self.favButton.setTitle("Unfavorite", for: .normal)
                self.favorite = true
            } else {
                defaults.removeObject(forKey: String(launch.id))
                self.favButton.setTitle("Favorite", for: .normal)
                self.favorite = false
            }
        }
    }

}
