//
//  ViewController.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import UIKit

class LaunchesViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    private var viewModel: LaunchesViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.isHidden = true
        
        let request = LaunchRequest()
        self.viewModel = LaunchesViewModel(request: request, delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.fetchLaunches()
    }

}

extension LaunchesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaunchCell") as? LaunchTableViewCell else {
            fatalError()
        }
        
        cell.configure(with: self.viewModel.launch(at: indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension LaunchesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let launchViewController = LaunchViewController.controllerFromStoryboard()
        launchViewController.launch = self.viewModel.launch(at: indexPath.row)
        self.navigationController?.pushViewController(launchViewController, animated: true)
    }
}

extension LaunchesViewController: LaunchesViewModelDelegate {
    func onFetchCompleted() {
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true
        self.tableView.isHidden = false
        self.tableView.reloadData()
    }
    
    func onFetchFailed(with reason: String) {
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true
        
        let title = "Error"
        let alertController = UIAlertController(title: title, message: reason, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}
