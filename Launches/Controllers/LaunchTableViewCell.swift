//
//  LaunchTableViewCell.swift
//  Launches
//
//  Created by Cyril PIVEC on 13/07/2019.
//  Copyright © 2019 Cyril PIVEC. All rights reserved.
//

import UIKit

class LaunchTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var view: UIView!
    @IBOutlet var statusView: UIView!
    @IBOutlet var favImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view.layer.cornerRadius = 6
        self.statusView.layer.cornerRadius = 7
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(with launch: Launch?) {
        if let launch = launch {
            self.nameLabel?.text = launch.name
            self.dateLabel?.text = launch.windowstart
            switch launch.status {
            case 1:
                self.statusView.backgroundColor = UIColor.green
            case 2:
                self.statusView.backgroundColor = UIColor.red
            case 3:
                self.statusView.backgroundColor = UIColor.yellow
            case 4:
                self.statusView.backgroundColor = UIColor.gray
            default:
                self.statusView.backgroundColor = UIColor.black
            }
            self.setFav(with: launch.id)
        }
    }
    
    func setFav(with id: Int) {
        let defaults = UserDefaults.standard
        if defaults.value(forKey: String(id)) != nil {
            self.favImageView.isHidden = false
        } else {
            self.favImageView.isHidden = true
        }
    }
}
